<?php

namespace App\Http\Controllers;

use App\Beverage;
use App\Cosmetic;
use App\HealthItem;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use File;

class DashboardController extends Controller
{

    public function index()
    {
        $cosmetics = Cosmetic::all();
        return view('admin.home',['cosmetics'=> $cosmetics]);
    }

    public function indexBeverage()
    {
        $beverages = Beverage::all();
        return view('admin.beveragedetails',['beverages'=> $beverages]);
    }

    public function indexHealth()
    {
        $health_items = HealthItem::all();
        return view('admin.health_items',['health_items'=> $health_items]);
    }

    public function indexOrder()
    {
        $orders = Order::all();
        return view('admin.orders',['orders'=> $orders]);
    }

    public function indexUsers()
    {
        $users = User::all();
        return view('admin.users',['users'=> $users]);
    }

    public function AddCosmetic(Request $request)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');

        $cosmetic = new Cosmetic();

        $cosmetic->name = $name;
        $cosmetic->description = $description;
        $cosmetic->price = $price;


        if(!empty($image)) {
            $file_ex = strtolower(File::extension($image->getClientOriginalName()));

            if($file_ex != "png" && $file_ex != "jpg" && $file_ex != "jpeg" && $file_ex != "gif") {
                $res['success'] = false;
                $res['message']= 'Invalid photo type!';
                return response( $res );
            }

            $filename = uniqid().".".$file_ex;
            $image->storeAs('public/cosmetics', $filename);

            $cosmetic->image = $filename;
        }

        $cosmetic->save();

        $res['success'] = true;
        $res['message'] = 'Product Added successfully!';
        return response($res);
    }

    public function editCosmetic(Request $request)
    {
        $id = $request->input('id');
        $cosmetic = Cosmetic::find($id);

        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');

        $cosmetic->name = $name;
        $cosmetic->description = $description;
        $cosmetic->price = $price;


        if(!empty($image)) {
            $file_ex = strtolower(File::extension($image->getClientOriginalName()));

            if($file_ex != "png" && $file_ex != "jpg" && $file_ex != "jpeg" && $file_ex != "gif") {
                $res['success'] = false;
                $res['message']= 'Invalid photo type!';
                return response( $res );
            }

            $filename = uniqid().".".$file_ex;
            $image->storeAs('public/cosmetics', $filename);

            $cosmetic->image = $filename;
        }

        $cosmetic->save();

        $res['success'] = true;
        $res['message'] = 'Product Updated successfully!';
        return response($res);
    }

    public function AddBeverage(Request $request)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');

        $beverage = new Beverage();

        $beverage->name = $name;
        $beverage->description = $description;
        $beverage->price = $price;


        if(!empty($image)) {
            $file_ex = strtolower(File::extension($image->getClientOriginalName()));

            if($file_ex != "png" && $file_ex != "jpg" && $file_ex != "jpeg" && $file_ex != "gif") {
                $res['success'] = false;
                $res['message']= 'Invalid photo type!';
                return response( $res );
            }

            $filename = uniqid().".".$file_ex;
            $image->storeAs('public/beverages', $filename);

            $beverage->image = $filename;
        }

        $beverage->save();

        $res['success'] = true;
        $res['message'] = 'Product Added successfully!';
        return response($res);
    }

    public function editBeverage(Request $request)
    {
        $id = $request->input('id');
        $cosmetic = Beverage::find($id);

        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');

        $cosmetic->name = $name;
        $cosmetic->description = $description;
        $cosmetic->price = $price;


        if(!empty($image)) {
            $file_ex = strtolower(File::extension($image->getClientOriginalName()));

            if($file_ex != "png" && $file_ex != "jpg" && $file_ex != "jpeg" && $file_ex != "gif") {
                $res['success'] = false;
                $res['message']= 'Invalid photo type!';
                return response( $res );
            }

            $filename = uniqid().".".$file_ex;
            $image->storeAs('public/beverages', $filename);

            $cosmetic->image = $filename;
        }

        $cosmetic->save();

        $res['success'] = true;
        $res['message'] = 'Product Updated successfully!';
        return response($res);
    }

    public function AddHealthItem(Request $request)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');

        $health_item = new HealthItem();

        $health_item->name = $name;
        $health_item->description = $description;
        $health_item->price = $price;


        if(!empty($image)) {
            $file_ex = strtolower(File::extension($image->getClientOriginalName()));

            if($file_ex != "png" && $file_ex != "jpg" && $file_ex != "jpeg" && $file_ex != "gif") {
                $res['success'] = false;
                $res['message']= 'Invalid photo type!';
                return response( $res );
            }

            $filename = uniqid().".".$file_ex;
            $image->storeAs('public/health', $filename);

            $health_item->image = $filename;
        }

        $health_item->save();

        $res['success'] = true;
        $res['message'] = 'Product Added successfully!';
        return response($res);
    }

    public function editHealthItem(Request $request)
    {
        $id = $request->input('id');
        $cosmetic = HealthItem::find($id);

        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');

        $cosmetic->name = $name;
        $cosmetic->description = $description;
        $cosmetic->price = $price;


        if(!empty($image)) {
            $file_ex = strtolower(File::extension($image->getClientOriginalName()));

            if($file_ex != "png" && $file_ex != "jpg" && $file_ex != "jpeg" && $file_ex != "gif") {
                $res['success'] = false;
                $res['message']= 'Invalid photo type!';
                return response( $res );
            }

            $filename = uniqid().".".$file_ex;
            $image->storeAs('public/health', $filename);

            $cosmetic->image = $filename;
        }

        $cosmetic->save();

        $res['success'] = true;
        $res['message'] = 'Product Updated successfully!';
        return response($res);
    }

    public function deleteCosmetic($id)
    {
        $cosmetic = Cosmetic::find($id);
        $cosmetic->delete();
        return redirect('home');
    }

    public function deleteBeverage($id)
    {
        $beverage = Beverage::find($id);
        $beverage->delete();
        return redirect('beverage_details');
    }

    public function deleteHealthItem($id)
    {
        $health_item = HealthItem::find($id);
        $health_item->delete();
        return redirect('health_items');
    }
}
