<?php

namespace App\Http\Controllers;

use App\Beverage;
use App\Cosmetic;
use App\HealthItem;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function showSingleProduct($type, $id)
    {
        if ($type === "beverages") {
            $item = Beverage::find($id);
        } else if ($type === "cosmetics") {
            $item = Cosmetic::find($id);
        } else if ($type === "health") {
            $item = HealthItem::find($id);
        }

        return view('product_details', ['item' => $item, 'type' => $type]);
    }

    public function showCheckoutPage($type, $id)
    {
        if ($type === "beverages") {
            $item = Beverage::find($id);
        } else if ($type === "cosmetics") {
            $item = Cosmetic::find($id);
        } else if ($type === "health") {
            $item = HealthItem::find($id);
        }

        return view('checkout', ['item' => $item, 'type' => $type]);
    }

    public function orderSave(Request $request)
    {

        $order = new Order();
        $order->product_name = $request->input('product_name');
        $order->customer_name = $request->input('customer_name');
        $order->product_name = $request->input('product_name');
        $order->email = $request->input('email');
        $order->address = $request->input('address');
        $order->quantity = $request->input('quantity');
        $order->note = $request->input('note');

        $order->save();

        try {

            $to_name = 'Biocare';
            $to_email = $request->input('email');
            $from_email = 'sakindunethul@gmail.com';
            $data = array('name'=>$request->input('customer_name'),
            );

            Mail::send('layouts.contactmail',$data, function ($message) use ($to_name, $to_email, $from_email) {
                $message->to($to_email, $to_name)
                    ->subject("Thank You For Place Order");
                $message->from($from_email);
            });

            $res['success'] = true;
            $res['message'] = 'Order Placed Successfully';
            return response($res);
        } catch (Exception $ex) {
            dd($ex);
        }
    }
}
