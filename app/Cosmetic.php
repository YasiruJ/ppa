<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cosmetic extends Model
{
    protected $table = 'cosmetics';
    protected $primarykey = 'id';
    protected $fillable = ['name','description','price','image'];
    public $timestamps = false;



}
