<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primarykey = 'id';
    protected $fillable = ['product_name','customer_name','email','address','quantity','note'];
    public $timestamps = false;
}
