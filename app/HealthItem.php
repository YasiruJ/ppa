<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthItem extends Model
{
    protected $table = 'health_items';
    protected $primarykey = 'id';
    protected $fillable = ['name','description','price','image'];
    public $timestamps = false;
}
