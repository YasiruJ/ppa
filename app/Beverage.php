<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beverage extends Model
{
    protected $table = 'beverages';
    protected $primarykey = 'id';
    protected $fillable = ['name','description','price','image'];
    public $timestamps = false;
}
