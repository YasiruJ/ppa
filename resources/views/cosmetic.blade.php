@extends('layouts.master')

@section('content')
    <div class="breadcrumb-area pt-10 pb-10 border-bottom mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  breadcrumb content  =======-->

                    <div class="breadcrumb-content">
                        <ul>
                            <li class="has-child"><a href="index.html">Home</a></li>
                            <li>Shop</li>
                        </ul>
                    </div>

                    <!--=======  End of breadcrumb content  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of breadcrumb area  ====================-->

    <!--==================== page content ====================-->

    <div class="page-section pb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 order-1">
                    <!--=======  shop banner  =======-->
                    <div class="row shop-product-wrap grid three-column mb-10">
                        @foreach ($cosmetics as $item)
                            <div class="col-12 col-lg-4 col-md-6 col-sm-6 mb-20">

                                <div class="single-slider-product grid-view-product">
                                    <div class="single-slider-product__image">
                                        <a href="{{URL('product/single/cosmetics')}}/{{$item->id}}">
                                            <img src="{{ URL('storage/cosmetics/') }}/{{$item->image}}" class="img-fluid" alt="">
                                        </a>

                                        <span class="discount-label discount-label--green">-10%</span>

                                        <div class="hover-icons">
                                            <ul>
                                                <li><a data-toggle = "modal" data-target="#quick-view-modal-container" href="javascript:void(0)"><i class="icon-eye"></i></a></li>
                                                <li><a href="javascript:void(0)"><i class="icon-heart"></i></a></li>
                                                <li><a href="javascript:void(0)"><i class="icon-sliders"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="single-slider-product__content">
                                        <p class="product-title"><a href="{{URL('product/single/cosmetics')}}/{{$item->id}}">{{$item->name}}</a></p>
                                        {{-- <div class="rating">
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star"></i>
                                        </div> --}}
                                        <p class="product-price"><span class="discounted-price">Rs {{$item->price}}</span></p>

                                        <span class="cart-icon"><a href="javascript:void(0)" onclick="addCart('cosmetics','{{$item->id}}','{{$item->name}}','{{$item->price}}','{{$item->image}}')"><i class="icon-shopping-cart"></i></a></span>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                            <!--=======  End of grid view product  =======-->

                            <!--=======  grid view product  =======-->

                            <div class="single-slider-product single-slider-product--list-view list-view-product">
                                <div class="single-slider-product__image single-slider-product--list-view__image">
                                    <a href="single-product.html">
                                        <img src="assets/img/products/medium11.jpg" class="img-fluid" alt="">
                                    </a>

                                    <span class="discount-label discount-label--green">-5%</span>
                                </div>

                                <div class="single-slider-product__content  single-slider-product--list-view__content">
                                    <div class="single-slider-product--list-view__content__details">
                                        <p class="product-title"><a href="single-product.html">Cillum dolore ipsum plant</a></p>
                                        <div class="rating">
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star active"></i>
                                            <i class="ion-android-star"></i>
                                        </div>

                                        <p class="short-desc">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Perspiciatis autem consequuntur tempora magnam possimus sunt.</p>
                                    </div>

                                    <div class="single-slider-product--list-view__content__actions">
                                        <div class="availability mb-10">
                                            <span class="availability-title">Availabe:</span>
                                            <span class="availability-value">Out of stock</span>
                                        </div>

                                        <p class="product-price"><span class="discounted-price">$40.00</span> <span class="main-price discounted">$100.00</span></p>

                                        <a href="#" class="theme-button list-cart-button mb-10">Add to Cart</a>

                                        <div class="hover-icons">
                                            <ul>
                                                <li><a data-toggle = "modal" data-target="#quick-view-modal-container" href="javascript:void(0)"><i class="icon-eye"></i></a></li>
                                                <li><a href="javascript:void(0)"><i class="icon-heart"></i></a></li>
                                                <li><a href="javascript:void(0)"><i class="icon-sliders"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of grid view product  =======-->

                        </div>
                    </div>
                </div>
            </div>
    </div>

    <!--====================  End of page content  ====================-->

@endsection
