@extends('layouts.master')

@section('content')
    <div class="breadcrumb-area pt-10 pb-10 border-bottom mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  breadcrumb content  =======-->

                    <div class="breadcrumb-content">
                        <ul>
                            <li class="has-child"><a href="index.html">Home</a></li>
                            <li class="has-child"><a href="shop-left-sidebar.html">Shop</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div>

                    <!--=======  End of breadcrumb content  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of breadcrumb area  ====================-->

    <!--====================  product details area ====================-->

    <div class="product-details-area mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-md-30 mb-sm-25">
                    <!--=======  product details slider  =======-->

                    <!--=======  big image slider  =======-->

                    <div class="big-image-slider-wrapper big-image-slider-wrapper--change-cursor">
                        <div class="ht-slick-slider big-image-slider99"
                             data-slick-setting='{
                                "slidesToShow": 1,
                                "slidesToScroll": 1,
                                "dots": false,
                                "autoplay": false,
                                "autoplaySpeed": 5000,
                                "speed": 1000
                            }'
                             data-slick-responsive='[
                                {"breakpoint":1501, "settings": {"slidesToShow": 1} },
                                {"breakpoint":1199, "settings": {"slidesToShow": 1} },
                                {"breakpoint":991, "settings": {"slidesToShow": 1} },
                                {"breakpoint":767, "settings": {"slidesToShow": 1} },
                                {"breakpoint":575, "settings": {"slidesToShow": 1} },
                                {"breakpoint":479, "settings": {"slidesToShow": 1} }
                            ]'
                        >

                            <!--=======  big image slider single item  =======-->

                            <div class="big-image-slider-single-item">
                                <img src="{{ URL('storage/') }}/{{$type}}/{{$item->image}}" class="img-fluid" alt="">
                            </div>

                            <!--=======  End of big image slider single item  =======-->

                            <!--=======  big image slider single item  =======-->


                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <!--=======  product details content  =======-->

                    <div class="product-detail-content">
                        <div class="tags mb-5">
                            <span class="tag-title">Tags:</span>
                            <ul class="tag-list">
                                <li><a href="#">Plant</a>,</li>
                                <li><a href="#">Garden</a></li>
                            </ul>
                        </div>

                        <h3 class="product-details-title mb-15">{{$item->name}}</h3>

                        <p class="product-price product-price--big mb-10"><span class="discounted-price">Rs {{$item->price}}.00</span> <span class="main-price discounted">Rs {{($item->price)+20}}.00</span></p>

                        <div class="product-short-desc mb-25">
                            <p>{{$item->description}}</p>
                        </div>

                        <div class="quantity mb-20">
                            <a class="theme-button product-cart-button" href="{{URL('product/checkout')}}/{{$type}}/{{$item->id}}">+ Place Order</a>
                        </div>


                        <div class="product-details-feature-wrapper d-flex justify-content-start mt-20">
                            <!--=======  single icon feature  =======-->

                            <div class="single-icon-feature single-icon-feature--product-details">
                                <div class="single-icon-feature__icon">
                                    <img src="assets/img/icons/free-shipping.png" class="img-fluid" alt="">
                                </div>
                                <div class="single-icon-feature__content">
                                    <p class="feature-text">Free Shipping</p>
                                    <p class="feature-text">Ships Today</p>
                                </div>
                            </div>

                            <!--=======  End of single icon feature  =======-->

                            <!--=======  single icon feature  =======-->

                            <div class="single-icon-feature single-icon-feature--product-details ml-30 ml-xs-0 ml-xxs-0">
                                <div class="single-icon-feature__icon">
                                    <img src="assets/img/icons/return.png" class="img-fluid" alt="">
                                </div>
                                <div class="single-icon-feature__content">
                                    <p class="feature-text">Easy</p>
                                    <p class="feature-text">Returns</p>
                                </div>
                            </div>

                            <!--=======  End of single icon feature  =======-->

                            <!--=======  single icon feature  =======-->

                            <div class="single-icon-feature single-icon-feature--product-details ml-30 ml-xs-0 ml-xxs-0">
                                <div class="single-icon-feature__icon">
                                    <img src="assets/img/icons/guarantee.png" class="img-fluid" alt="">
                                </div>
                                <div class="single-icon-feature__content">
                                    <p class="feature-text">Lowest Price</p>
                                    <p class="feature-text">Guarantee</p>
                                </div>
                            </div>

                            <!--=======  End of single icon feature  =======-->
                        </div>

                        <div class="social-share-buttons mt-20">
                            <h3>share this product</h3>
                            <ul>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>

                    </div>

                    <!--=======  End of product details content  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of product details area  ====================-->



@endsection
