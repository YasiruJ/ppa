
<!--====================  Header area ====================-->

<div class="header-area header-sticky">
    <!--====================  Navigation top ====================-->

    <div class="navigation-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--====================  navigation section ====================-->

                    <div class="navigation-top-topbar pt-10 pb-10">
                        <div class="row align-items-center">
                            <div class="col-lg-4 col-md-6 text-center text-md-left">
                                <!--=======  header top social links  =======-->

                                <div class="header-top-social-links">
                                    <span class="follow-text mr-15">Follow Us:</span>
                                    <!--=======  social link small  =======-->

                                    <ul class="social-link-small">
                                        <li><a href="http://www.facebook.com/"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="http://www.twitter.com/"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="http://plus.google.com/"><i class="ion-social-googleplus-outline"></i></a></li>
                                        <li><a href="http://www.instagram.com/"><i class="ion-social-instagram-outline"></i></a></li>
                                        <li><a href="http://www.youtube.com/"><i class="ion-social-youtube"></i></a></li>
                                    </ul>

                                    <!--=======  End of social link small  =======-->
                                </div>


                                <!--=======  End of header top social links  =======-->
                            </div>
                            <div class="col-lg-4 offset-lg-4 col-md-6">
                                <!--=======  header top dropdown container  =======-->

                            {{--                                <div class="headertop-dropdown-container justify-content-center justify-content-md-end">--}}
                            {{--                                    <div class="header-top-single-dropdown">--}}
                            {{--                                        <a href="javascript:void(0)" class="active-dropdown-trigger" id="user-options">My Account <i class="ion-ios-arrow-down"></i></a>--}}
                            {{--                                        <!--=======  dropdown menu items  =======-->--}}

                            {{--                                        <div class="header-top-single-dropdown__dropdown-menu-items deactive-dropdown-menu extra-small-mobile-fix">--}}
                            {{--                                            <ul>--}}
                            {{--                                                <li><a href="{{URL('/register')}}">Register</a></li>--}}
                            {{--                                                <li><a href="{{URL('/login')}}">Login</a></li>--}}
                            {{--                                            </ul>--}}
                            {{--                                        </div>--}}

                            {{--                                        <!--=======  End of dropdown menu items  =======-->--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}

                            <!--=======  End of header top dropdown container  =======-->
                            </div>
                        </div>
                    </div>

                    <!--====================  End of navigation section  ====================-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!--====================  navigation top search ====================-->


                    <div class="navigation-top-search-area pt-25 pb-25">
                        <div class="row align-items-center">
                            <div class="col-6 col-xl-2 col-lg-3 order-1 col-md-6 col-sm-5">
                                <!--=======  logo  =======-->

                                <div class="logo">
                                    <a href="{{URL('/')}}">
                                        <h1 style="color: white;">Biocare Consumers</h1>
                                    </a>
                                </div>

                                <!--=======  End of logo  =======-->
                            </div>

                            <div class="col-xl-5 offset-xl-1 col-lg-4 order-4 order-lg-2 mt-md-25 mt-sm-25">
                                <!--=======  search bar  =======-->

                            {{-- <div class="search-bar">
                                <form action="#">
                                    <input type="search" placeholder="Search entire store here ...">
                                    <button type="submit"> <i class="icon-search"></i></button>
                                </form>
                            </div> --}}

                            <!--=======  End of search bar  =======-->
                            </div>

                            <div class="col-xl-3 col-lg-3 order-3 order-sm-2 order-lg-3 order-xs-3 col-md-4 col-sm-5 text-center text-sm-left mt-xs-25">
                                <!--=======  customer support text  =======-->

                            {{-- <div class="customer-support-text">
                                <div class="icon">
                                    <img src="assets/img/icons/icon-header-phone.png" class="img-fluid" alt="">
                                </div>

                                <div class="text">
                                    <span>Customer Support</span>
                                    <p>(08) 12 345 789</p>
                                </div>
                            </div> --}}

                            <!--=======  End of customer support text  =======-->
                            </div>

                            <div class="col-6 col-xl-1 col-lg-2 text-right order-2 order-sm-3 order-lg-4 order-xs-2 col-md-2 col-sm-2">
                                <!--=======  cart icon  =======-->

                                <div class="header-cart-icon">
                                    <a href="javascript:void(0)" id="small-cart-trigger" class="small-cart-trigger">
                                        <i class="icon-shopping-cart"></i>
                                        <span class="cart-counter"></span>
                                    </a>

                                    <!--=======  small cart  =======-->

                                    <div class="small-cart deactive-dropdown-menu">
                                        <div class="small-cart-item-wrapper">



                                        </div>

                                    </div>

                                    <!--=======  End of small cart  =======-->
                                </div>

                                <!--=======  End of cart icon  =======-->
                            </div>
                        </div>
                    </div>

                    <!--====================  End of navigation top search  ====================-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of Navigation top  ====================-->

    <!--====================  navigation menu ====================-->

    <div class="navigation-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- navigation section -->
                    <div class="main-menu d-none d-lg-block">
                        <nav>
                            <ul>
                                <li><a href="{{URL('/')}}">HOME</a></li>
                                <li><a href="{{URL('/cosmetics')}}">COSMETICS</a></li>
                                <li><a href="{{URL('/beverages')}}">BEVERAGES</a></li>
                                <li><a href="{{URL('/health')}}">HEALTH ITEMS</a></li>


                            </ul>
                        </nav>

                    </div>
                    <!-- end of navigation section -->

                    <!-- Mobile Menu -->
                    <div class="mobile-menu-wrapper d-block d-lg-none pt-15">
                        <div class="mobile-menu"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of navigation menu  ====================-->
</div>

<!--====================  End of Header area  ====================-->
