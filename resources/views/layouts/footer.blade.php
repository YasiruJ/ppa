
<!--====================  footer area ====================-->

<div class="footer-area">
    <div class="container">
        <div class="row mb-40">
            <div class="col-lg-12">
                <div class="footer-content-wrapper border-top pt-40">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <!--=======  single footer widget  =======-->

                            <div class="single-footer-widget">
                                <div class="footer-logo mb-25">
                                    <a href="{{URL('/')}}">
                                        <h1>Biocare Consumers</h1>
                                    </a>
                                </div>

                                <div class="footer-text-block mb-10">
                                    <h5 class="footer-text-block__title">Address</h5>
                                    <p class="footer-text-block__content">No 14/A,Galle Road, Panadura</p>
                                </div>

                                <div class="footer-text-block mb-10">
                                    <h5 class="footer-text-block__title">Need Help?</h5>
                                    <p class="footer-text-block__content">Call: 038-2235678</p>
                                </div>


                            </div>

                            <!--=======  End of single footer widget  =======-->
                        </div>

                        <div class="col-lg-4 col-md-6 mt-sm-30">
                            <!--=======  single footer widget  =======-->



                            <!--=======  End of single footer widget  =======-->
                        </div>


                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<!--====================  End of footer area  ====================-->
