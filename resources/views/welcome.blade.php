@extends('layouts.master')

@section('content')

    <div class="hero-slider-area mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  hero slider wrapper  =======-->

                    <div class="hero-slider-wrapper">
                        <div class="ht-slick-slider"
                             data-slick-setting='{
                            "slidesToShow": 1,
                            "slidesToScroll": 1,
                            "arrows": false,
                            "dots": true,
                            "autoplay": true,
                            "autoplaySpeed": 5000,
                            "speed": 1000,
                            "fade": true
                        }'
                             data-slick-responsive='[
                            {"breakpoint":1501, "settings": {"slidesToShow": 1} },
                            {"breakpoint":1199, "settings": {"slidesToShow": 1} },
                            {"breakpoint":991, "settings": {"slidesToShow": 1} },
                            {"breakpoint":767, "settings": {"slidesToShow": 1} },
                            {"breakpoint":575, "settings": {"slidesToShow": 1} },
                            {"breakpoint":479, "settings": {"slidesToShow": 1} }
                        ]'
                        >

                            <!--=======  single slider item  =======-->

                            <div class="single-slider-item">
                                <div class="hero-slider-item-wrapper hero-slider-bg-1">
                                    <div class="hero-slider-content pl-60 pl-sm-30">
                                        <p class="slider-title slider-title--small">Aloe vera Gel</p>
                                        <p class="slider-title slider-title--big-bold">Aloe Vera Skin Care</p>
                                        <p class="slider-title slider-title--big-light">To Your Skin</p>
                                        <a class="theme-button hero-slider-button" href="shop-left-sidebar.html">Shopping Now</a>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single slider item  =======-->

                            <!--=======  single slider item  =======-->

                            <div class="single-slider-item">
                                <div class="hero-slider-item-wrapper hero-slider-bg-2">
                                    <div class="hero-slider-content pl-60 pl-sm-30">
                                        <p class="slider-title slider-title--small">Workshops @Alula</p>
                                        <p class="slider-title slider-title--big-bold">Plant Make</p>
                                        <p class="slider-title slider-title--big-light">People Happy</p>
                                        <a class="theme-button hero-slider-button" href="shop-left-sidebar.html">Shopping Now</a>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single slider item  =======-->

                            <!--=======  single slider item  =======-->

                            <div class="single-slider-item">
                                <div class="hero-slider-item-wrapper hero-slider-bg-3">
                                    <div class="hero-slider-content pl-60 pl-sm-30">
                                        <p class="slider-title slider-title--small">Thursday Through Saturday</p>
                                        <p class="slider-title slider-title--big-bold">Plant Big Sale</p>
                                        <p class="slider-title slider-title--big-light">75% Off</p>
                                        <a class="theme-button hero-slider-button" href="shop-left-sidebar.html">Shopping Now</a>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single slider item  =======-->

                        </div>
                    </div>

                    <!--=======  End of hero slider wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of hero slider area  ====================-->

    <!--====================  split banner area ====================-->



    <!--====================  End of split banner area  ====================-->
    <!--====================  product single row counter slider area ====================-->



    <!--====================  End of product single row counter slider area  ====================-->
    <!--====================  full banner area ====================-->

    {{-- <div class="full-banner-area mb-40 mb-md-10 mb-sm-10">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-4 col-lg-5 mb-md-30 mb-sm-30">
                    <!--=======  full banner content  =======-->

                    <div class="full-banner__content">
                        <h5>special offer</h5>
                        <h4>SUCCULENT GARDEN</h4>
                        <h3>GIFT BOXES</h3>

                        <p>From planter materials to style options, discover which planter is best for your space.</p>

                        <a href="shop-left-sidebar.html" class="theme-button theme-button--outline banner-button">Explore The Shop</a>
                    </div>
                    <!--=======  End of full banner content  =======-->
                </div>
                <div class="col-xl-8 col-lg-7 text-center text-lg-right mb-md-30 mb-sm-30">
                    <!--=======  full banner image  =======-->
                    <div class="full-banner__image">
                        <a href="shop-left-sidebar.html">
                            <img src="assets/img/banners/banner-big1.jpg" class="img-fluid" alt="">
                        </a>
                    </div>

                    <!--=======  End of full banner image  =======-->
                </div>
            </div>
        </div>
    </div> --}}



    <div class="icon-feature-area mb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  icon feature wrapper  =======-->

                    <div class="icon-feature-wrapper">
                        <div class="row row-5">
                            <div class="col-6 col-lg-3 col-sm-6">
                                <!--=======  single icon feature  =======-->

                                <!--=======  End of single icon feature  =======-->
                            </div>
                            <div class="col-6 col-lg-3 col-sm-6">
                                <!--=======  single icon feature  =======-->

                                <div class="single-icon-feature">
                                    <div class="single-icon-feature__icon">
                                        <img src="assets/img/icons/support247.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="single-icon-feature__content">
                                        <p class="feature-title">Support 24/7</p>
                                        <p class="feature-text">Contact us 24 hrs a day</p>
                                    </div>
                                </div>

                                <!--=======  End of single icon feature  =======-->
                            </div>
                            <div class="col-6 col-lg-3 col-sm-6">
                                <!--=======  single icon feature  =======-->

                                <div class="single-icon-feature">
                                    <div class="single-icon-feature__icon">
                                        <img src="assets/img/icons/moneyback.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="single-icon-feature__content">
                                        <p class="feature-title">100% Money Back</p>
                                        <p class="feature-text">You’ve 30 days to Return</p>
                                    </div>
                                </div>

                                <!--=======  End of single icon feature  =======-->
                            </div>

                        </div>
                    </div>

                    <!--=======  End of icon feature wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of icon feature area  ====================-->
    <!--====================  category area ====================-->

    <div class="category-area mb-40">
        <div class="container">
            <!--=======  section title  =======-->

            <div class="section-title mb-20">
                <h2>Featured Categories</h2>
            </div>

            <!--=======  End of section title  =======-->

            <div class="row">
                <div class="col-lg-12">
                    <!--=======  category slider wrapper  =======-->

                    <div class="category-slider-wrapper-one">
                        <div class="ht-slick-slider"
                             data-slick-setting='{
                                "slidesToShow": 4,
                                "slidesToScroll": 1,
                                "dots": false,
                                "autoplay": false,
                                "autoplaySpeed": 5000,
                                "speed": 1000
                            }'
                             data-slick-responsive='[
                                {"breakpoint":1501, "settings": {"slidesToShow": 4} },
                                {"breakpoint":1199, "settings": {"slidesToShow": 4} },
                                {"breakpoint":991, "settings": {"slidesToShow": 3} },
                                {"breakpoint":767, "settings": {"slidesToShow": 2, "arrows": false} },
                                {"breakpoint":575, "settings": {"slidesToShow": 2, "arrows": false} },
                                {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false} }
                            ]'
                        >

                            <!--=======  single category item  =======-->

                            <div class="single-category-item">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="assets/img/category/cat1.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="single-category-item__image__content">
                                        <h5 class="category-title"><a href="#">BONSAI</a></h5>
                                        <p class="quntity">6 products</p>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single category item  =======-->

                            <!--=======  single category item  =======-->

                            <div class="single-category-item">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="assets/img/category/cat2.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="single-category-item__image__content">
                                        <h5 class="category-title"><a href="#">HOUSE PLANTS</a></h5>
                                        <p class="quntity">8 products</p>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single category item  =======-->

                            <!--=======  single category item  =======-->

                            <div class="single-category-item">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="assets/img/category/cat3.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="single-category-item__image__content">
                                        <h5 class="category-title"><a href="#">PERENNIALS</a></h5>
                                        <p class="quntity">7 products</p>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single category item  =======-->

                            <!--=======  single category item  =======-->

                            <div class="single-category-item">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="assets/img/category/cat4.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="single-category-item__image__content">
                                        <h5 class="category-title"><a href="#">PLANT FOR GIFT</a></h5>
                                        <p class="quntity">5 products</p>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single category item  =======-->

                            <!--=======  single category item  =======-->

                            <div class="single-category-item">
                                <div class="single-category-item__image">
                                    <a href="#">
                                        <img src="assets/img/category/cat1.jpg" class="img-fluid" alt="">
                                    </a>
                                    <div class="single-category-item__image__content">
                                        <h5 class="category-title"><a href="#">BONSAI</a></h5>
                                        <p class="quntity">6 products</p>
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single category item  =======-->

                        </div>

                    </div>

                    <!--=======  End of category slider wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

@endsection
