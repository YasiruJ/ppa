@extends('layouts.app')



@section('content')


    <!-- Page Content -->
    <div id="page-content-wrapper">

    </div>
    <!-- /#wrapper -->

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <a href="{{URL('/home')}}" class="list-group-item list-group-item-action bg-light">Cosmetics</a>
                <a href="{{URL('/beverage_details')}}" class="list-group-item list-group-item-action bg-light">Beverages</a>
                <a href="{{URL('/health_items')}}" class="list-group-item list-group-item-action bg-light">Health Items</a>
                <a href="{{URL('/orders')}}" class="list-group-item list-group-item-action bg-light">Orders</a>
                <a href="{{URL('/users')}}" class="list-group-item list-group-item-action bg-light">Users</a>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Health Item
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <form id="AddHealthForm" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product Name</label>
                                    <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Enter product Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product Description</label>
                                    {{-- <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"> --}}
                                    <textarea class="form-control" id="description" name="description" placeholder="Enter Product Description"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Price</label>
                                    <input type="text" class="form-control" id="price" name="price" aria-describedby="emailHelp" placeholder="Enter price">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Upload Image</label>
                                    <input type="file" class="form-control" id="image" name="image" />
                                </div>
                                <input type="hidden" name="id" id="id">
                                <div id="addBtn"><button type="submit" class="btn btn-primary" onclick="addHealth()">Add Health Item</button></div>
                                <div id="editBtn" style="display: none"><button type="submit" class="btn btn-primary" onclick="editHealth()">Edit Health Item</button></div>

                                <div id="success" style="display: none;">
                                    <label>HealthItem Added Successfully</label>
                                </div>
                                <div id="edit_success" style="display: none;">
                                    <label>Product Updated Successfully</label>
                                </div>
                            </form>
                        </div>
                    </div>



                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($health_items as $health_item)
                            <tr>
                                <td>{{$health_item->name}}</td>
                                <td>{{$health_item->price}}</td>
                                <td>
                                    <a href="{{URL('health_item/delete')}}/{{ $health_item->id }}"><input type="button" value="Delete" class="pull-right btn btn-sm btn-rounded btn-danger"></a>
                                    <input type="button" value="edit" class="pull-right btn btn-sm btn-rounded btn-warning" onclick="showEditBtn('{{ $health_item->id }}','{{$health_item->name}}','{{$health_item->description}}','{{$health_item->price}}','{{$health_item->image}}')">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>


            {{-- <div class="col-md-4"></div> --}}
            <div></div>
        </div>

        <div class="row">

        </div>
    </div>

@endsection
