@extends('layouts.app')



@section('content')
    {{-- <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    {{-- <div class="d-flex" id="wrapper">

      <!-- Sidebar -->
      <div class="bg-light border-right" id="sidebar-wrapper">
        {{-- <div class="sidebar-heading">Start Bootstrap </div> --}}
    {{-- <div class="list-group list-group-flush">
      <a href="#" class="list-group-item list-group-item-action bg-light">Dashboard</a>
      <a href="#" class="list-group-item list-group-item-action bg-light">Shortcuts</a>
      <a href="#" class="list-group-item list-group-item-action bg-light">Overview</a>
      <a href="#" class="list-group-item list-group-item-action bg-light">Events</a>
      <a href="#" class="list-group-item list-group-item-action bg-light">Profile</a>
      <a href="#" class="list-group-item list-group-item-action bg-light">Status</a>
    </div>
  </div> --}}
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
    {{-- <div class="col-md-10 col-offset-1">
        <form>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
      </div>
    </div>

    </div> --}}
    <!-- /#wrapper -->

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <a href="{{URL('/home')}}" class="list-group-item list-group-item-action bg-light">Cosmetics</a>
                    <a href="{{URL('/beverage_details')}}" class="list-group-item list-group-item-action bg-light">Beverages</a>
                    <a href="{{URL('/health_items')}}" class="list-group-item list-group-item-action bg-light">Health Items</a>
                    <a href="{{URL('/orders')}}" class="list-group-item list-group-item-action bg-light">Orders</a>
                    <a href="{{URL('/users')}}" class="list-group-item list-group-item-action bg-light">Users</a>

                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            Admin Users
                        </div>
                        <div class="card-body">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>




                    </div>


                </div>


                {{-- <div class="col-md-4"></div> --}}
                <div></div>
            </div>

            <div class="row">

            </div>
        </div>

@endsection
