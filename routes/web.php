<?php

use App\Beverage;
use App\Cosmetic;
use App\HealthItem;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cosmetics', function () {
    $cosmetics = Cosmetic::all();
    return view('cosmetic',['cosmetics'=> $cosmetics]);
});


Route::get('/beverages', function () {
    $beverages = Beverage::all();
    return view('beverage',['beverages'=> $beverages]);
});

Route::get('/health', function () {
    $health_items = HealthItem::all();
    return view('health',['health_items'=> $health_items]);
});

Route::get('product/single/{type}/{id}',[HomeController::class,'showSingleProduct']);

Auth::routes();

Route::get('/home', 'DashboardController@index')->name('home');

Route::get('/beverage_details', 'DashboardController@indexBeverage')->name('home');
Route::get('/health_items', 'DashboardController@indexHealth')->name('home');
Route::get('/orders', 'DashboardController@indexOrder')->name('home');
Route::get('/users', 'DashboardController@indexUsers')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');


Route::post('/product/add','DashboardController@AddCosmetic');
Route::post('/product/edit','DashboardController@editCosmetic');

Route::post('/beverage/add','DashboardController@AddBeverage');
Route::post('/beverage/edit','DashboardController@editBeverage');

Route::post('/health_item/add','DashboardController@AddHealthItem');
Route::post('/health_item/edit','DashboardController@editHealthItem');

Route::post('/order/save','HomeController@orderSave');
Route::get('product/checkout/{type}/{id}','HomeController@showCheckoutPage');

Route::get('/cosmetic/delete/{id}', 'DashboardController@deleteCosmetic');
Route::get('/beverage/delete/{id}', 'DashboardController@deleteBeverage');
Route::get('/health_item/delete/{id}', 'DashboardController@deleteHealthItem');
